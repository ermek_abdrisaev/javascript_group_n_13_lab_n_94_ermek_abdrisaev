import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiCocktailData, Cocktail, CocktailData, PubCock } from '../models/cocktail.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CocktailsService {

  constructor(private http: HttpClient) { }

  getCocktails(){
    return this.http.get<ApiCocktailData[]>(environment.apiUrl + '/cocktails').pipe(
      map(response =>{
        return response.map(cocktailData => {
          return new Cocktail(
            cocktailData._id,
            cocktailData.tag,
            cocktailData.image,
            cocktailData.recipe,
            cocktailData.ingredients,
            cocktailData.isPublished,
          )
        })
      })
    );
  }

  getUsersCocktails(id: string){
    return this.http.get<ApiCocktailData[]>(environment.apiUrl + `/cocktails?user=${id}`).pipe(
      map(response => {
        console.log(response);
        return response.map(cocktailData => {
          return new Cocktail(
          cocktailData._id,
          cocktailData.tag,
          cocktailData.image,
          cocktailData.recipe,
          cocktailData.ingredients,
          cocktailData.isPublished,
          )
        })
      })
    )
  }

  getCocktail(id: string) {
    return this.http.get<ApiCocktailData>(environment.apiUrl + `/cocktails/${id}`).pipe(
      map(result => {
          return result;
      })
    );
  }

  createCocktail(cocktailData: CocktailData){
    const formData = new FormData();

    Object.keys(cocktailData).forEach(key => {
      if(cocktailData[key] !== null) {
        if(key !== 'ingredients'){
          formData.append(key, cocktailData[key]);
        }else {
          formData.append(key, JSON.stringify(cocktailData[key]))
        }
      }
    });
    return this.http.post(environment.apiUrl + '/cocktails', formData);
  }

  publishCocktail(publishCocktail: PubCock){
    const publish = {
      isPublished: true
    }
    return this.http.post<PubCock>(environment.apiUrl + `/cocktails/${publishCocktail.id}/publish`, publish);
  }

  removeCocktail(id: string){
    return this.http.delete(environment.apiUrl + `/cocktails/${id}`);
  }

}
