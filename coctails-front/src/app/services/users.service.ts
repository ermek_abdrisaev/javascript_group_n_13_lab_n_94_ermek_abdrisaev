import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { LoginUserData, RegisterUserData, User } from '../models/user.model';
import { SocialUser } from 'angularx-social-login';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  registerUser(registerUserData: RegisterUserData) {
    const formData = new FormData();
    formData.append('email', registerUserData.email);
    formData.append('displayName', registerUserData.displayName);
    formData.append('password', registerUserData.password);

    if (registerUserData.avatar) {
      formData.append('avatar', registerUserData.avatar);
    }
    return this.http.post<User>(environment.apiUrl + '/users', formData);
  }

  login(userData: LoginUserData) {
    return this.http.post<User>(environment.apiUrl + '/users/sessions', userData);
  }

  fbLogin(user: SocialUser){
    return this.http.post<User>(environment.apiUrl + '/users/facebookLogin', user);
  }

  logout() {
    return this.http.delete(environment.apiUrl + '/users/sessions');
  }
}
