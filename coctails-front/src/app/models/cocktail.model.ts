export class Cocktail {
  constructor(
    public _id: string,
    public tag: string,
    public image: string,
    public recipe: string,
    public ingredients: [{
      ingName: string,
      ingAmmount: string,
    }],
    public isPublished: boolean,
  ) {}
}

export interface CocktailData {
  [key: string]: any,
  user: string,
  tag: string,
  image: null | File,
  recipe: string,
  isPublished: boolean,
  ingredients: [{
    ingName: string,
    ingAmmount: string,
  }],
}

export interface ApiCocktailData {
  _id: string,
  tag: string,
  image: string,
  recipe: string,
  ingredients: [{
    ingName: string,
    ingAmmount: string,
  }],
  isPublished: boolean,
}

export interface PubCock {
  id: string,
  isPublished: boolean
}
