import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuardService } from './services/role-guard.service';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { CocktailsNewComponent } from './pages/cocktails-new/cocktails-new.component';
import { CocktailsComponent } from './pages/cocktails/cocktails.component';
import { CocktailViewComponent } from './pages/cocktails/cocktail-view/cocktail-view.component';
import { MyCocktailsComponent } from './pages/cocktails/my-cocktails/my-cocktails.component';

const routes: Routes = [
  {path: '', component: CocktailsComponent},
  {
    path: 'new-cocktails',
    component: CocktailsNewComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
  {path: 'cocktail/:id', component: CocktailViewComponent},
  {path: 'userCocktails/:id', component: MyCocktailsComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
