import { localStorageSync } from 'ngrx-store-localstorage';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { usersReducer } from './store/users.reducer';
import { UsersEffects } from './store/users.effects';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { cocktailsReducer } from './store/cocktails.reducer';
import { CocktailsEffect } from './store/cocktails.effects';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true,
  })(reducer);
};

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

const reducers = {
  users: usersReducer,
  cocktails: cocktailsReducer,
};

const effects = [UsersEffects, CocktailsEffect];

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ],
  exports: [StoreModule, EffectsModule],
})
export class AppStoreModule {}
