import { createAction, props } from '@ngrx/store';
import { Cocktail, CocktailData, PubCock } from '../models/cocktail.model';

export const fetchCocktailsRequest = createAction(
  '[Cocktails] Fetch Request');
export const fetchCocktailsSuccess = createAction(
  '[Cocktails] Fetch Success',
  props<{cocktails: Cocktail[]}>()
);
export const fetchCocktailsFailure = createAction(
  '[Cocktails] Fetch Failure',
  props<{error: string}>()
);

export const fetchUsersCocktailsRequest = createAction(
  '[Cocktails] Fetch Users Request',
  props<{id: string}>()
);
export const fetchUsersCocktailsSuccess = createAction(
  '[Cocktails] Fetch Users Success',
  props<{cocktails: Cocktail[]}>()
);
export const fetchUsersCocktailsFailure = createAction(
  '[Cocktails] Fetch Users Failure',
  props<{error: string}>()
);



export const fetchCocktailRequest = createAction(
  '[Cocktails] FetchOne Request',
  props<{id: string}>()
  );
export const fetchCocktailSuccess = createAction(
  '[Cocktails] FetchOne Success',
  props<{cocktail: Cocktail}>()
);
export const fetchCocktailFailure = createAction(
  '[Cocktails] FetchOne Failure',
  props<{error: string}>()
);


export const createCocktailsRequest = createAction(
  '[Cocktails] Create Request',
  props<{cocktailData: CocktailData}>()
);
export const createCocktailsSuccess = createAction(
  '[Cocktails] Create Success'
);
export const createCocktailsFailure = createAction(
  '[Cocktails] Create Failure',
  props<{error: string}>()
);


export const publishCocktailRequest = createAction(
  '[Cocktail] Publish Request',
  props<{ publish: PubCock }>());
export const publishCocktailSuccess = createAction(
  '[Cocktail] Publish Success');
export const publishCocktailFailure = createAction(
  '[Cocktail] Publish Failure',
  props<{ error: string }>());


export const deleteCocktailRequest = createAction(
  '[Cocktail] Delete Request',
  props<{ id: string }>());
export const deleteCocktailSuccess = createAction(
  '[Cocktail] Delete Success');
export const deleteCocktailFailure = createAction(
  '[Cocktail] Delete Failure',
  props<{ error: string }>());
