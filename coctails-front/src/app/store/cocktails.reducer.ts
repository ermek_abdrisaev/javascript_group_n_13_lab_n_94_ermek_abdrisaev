import { CocktailState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createCocktailsFailure,
  createCocktailsRequest,
  createCocktailsSuccess, deleteCocktailFailure,
  deleteCocktailRequest,
  deleteCocktailSuccess,
  fetchCocktailFailure,
  fetchCocktailRequest,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchCocktailSuccess, fetchUsersCocktailsFailure, fetchUsersCocktailsRequest, fetchUsersCocktailsSuccess,
  publishCocktailFailure,
  publishCocktailRequest,
  publishCocktailSuccess
} from './cocktails.actions';
import { Cocktail } from '../models/cocktail.model';

const initialState: CocktailState = {
  cocktails: [],
  cocktail: null,
  pubCock: false,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const cocktailsReducer = createReducer(
  initialState,
  on(fetchCocktailsRequest, state => ({...state, fetchLoading: true})),
  on(fetchCocktailsSuccess, (state, {cocktails}) => ({...state, fetchLoading: false, cocktails})),
  on(fetchCocktailsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchUsersCocktailsRequest, state => ({...state, fetchLoading: true})),
  on(fetchUsersCocktailsSuccess, (state, {cocktails}) => ({...state, fetchLoading: false, cocktails})),
  on(fetchUsersCocktailsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchCocktailRequest, state => ({...state, fetchLoading: true})),
  on(fetchCocktailSuccess, (state, {cocktail}) => ({...state, fetchLoading: false, cocktail})),
  on(fetchCocktailFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createCocktailsRequest, state => ({...state, createLoading: true})),
  on(createCocktailsSuccess, state => ({...state, createLoading: false})),
  on(createCocktailsFailure,  (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(publishCocktailRequest, state => ({...state, createLoading: true})),
  on(publishCocktailSuccess, state => ({...state, createLoading: false})),
  on(publishCocktailFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(deleteCocktailRequest, state => ({...state, createLoading: true})),
  on(deleteCocktailSuccess, state => ({...state, createLoading: false})),
  on(deleteCocktailFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
)
