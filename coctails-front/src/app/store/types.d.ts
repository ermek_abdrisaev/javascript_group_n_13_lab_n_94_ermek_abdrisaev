import { LoginError, RegisterError, User } from '../models/user.model';
import { Cocktail } from '../models/cocktail.model';
import { SocialUser } from 'angularx-social-login';

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
}

export type CocktailState = {
  cocktails: Cocktail[],
  cocktail: Cocktail | null,
  pubCock: boolean,
  createLoading: boolean,
  createError: null | string,
  fetchLoading: boolean,
  fetchError: null | string,
}

export type AppState ={
  users: UsersState,
  cocktails: CocktailState,
 }
