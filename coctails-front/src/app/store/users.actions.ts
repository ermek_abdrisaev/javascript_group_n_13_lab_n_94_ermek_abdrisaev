import { createAction, props } from '@ngrx/store';
import { LoginError, LoginUserData, RegisterError, RegisterUserData, User } from '../models/user.model';
import { SocialUser } from 'angularx-social-login';

export const registerUserRequest = createAction(
  '[Users] Register Request',
  props<{userData: RegisterUserData}>()
  );
export const registerUserSuccess = createAction(
  '[Users] Register Success',
  props<{user: User}>()
  );
export const registerUserFailure = createAction(
  '[Users] Register Failure',
  props<{error: null | RegisterError}>()
  );

export const loginUserRequest = createAction(
  '[Users] Login Request',
  props<{userData: LoginUserData}>()
);
export const loginUserSuccess = createAction(
  '[Users] Login Success',
  props<{user: User}>()
);
export const loginUserFailure = createAction(
  '[Users] Login Failure',
  props<{error: null | LoginError}>()
);

export const logoutUser = createAction(
  '[Users] Logout');

export const logoutUserRequest = createAction(
  '[Users] Server Logout Request');

export const fbLoginRequest = createAction(
  '[User] Login Request',
  props<{user: SocialUser}>());
export const fbLoginSuccess = createAction(
  '[User] Login Success',
  props<{user: User}>());
export const fbLoginFailure = createAction(
  '[User] Login Failure',
  props<{error: null | LoginError}>());
