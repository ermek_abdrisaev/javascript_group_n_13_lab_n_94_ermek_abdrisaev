import { Injectable } from '@angular/core';
import { CocktailsService } from '../services/cocktails.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import {
  createCocktailsFailure,
  createCocktailsRequest,
  createCocktailsSuccess, deleteCocktailFailure,
  deleteCocktailRequest,
  deleteCocktailSuccess,
  fetchCocktailFailure,
  fetchCocktailRequest,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchCocktailSuccess, fetchUsersCocktailsFailure, fetchUsersCocktailsRequest, fetchUsersCocktailsSuccess,
  publishCocktailFailure,
  publishCocktailRequest,
  publishCocktailSuccess
} from './cocktails.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';

@Injectable()
export class CocktailsEffect {
  constructor(
    private actions: Actions,
    private cocktailService: CocktailsService,
    private router: Router,
    private store: Store
  ) {
  }

  fetchCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchCocktailsRequest),
    mergeMap(() => this.cocktailService.getCocktails().pipe(
      map(cocktails => fetchCocktailsSuccess({cocktails})),
      catchError(() => of(fetchCocktailsFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchCocktail = createEffect(() => this.actions.pipe(
    ofType(fetchCocktailRequest),
    mergeMap(({id}) => this.cocktailService.getCocktail(id).pipe(
      map(cocktail => fetchCocktailSuccess({cocktail})),
      catchError(() => of(fetchCocktailFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchUsersCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchUsersCocktailsRequest),
    mergeMap(({id}) => this.cocktailService.getUsersCocktails(id).pipe(
      map(cocktails => fetchUsersCocktailsSuccess({cocktails})),
      catchError(() => of(fetchUsersCocktailsFailure({error: 'User doesnt have cocktails'})))
    ))
  ));

  createCocktail = createEffect(() => this.actions.pipe(
    ofType(createCocktailsRequest),
    mergeMap(({cocktailData}) => this.cocktailService.createCocktail(cocktailData).pipe(
        map(() => createCocktailsSuccess()),
        tap(() => {
          return this.router.navigate(['/']);
        }),
        catchError(() => {
          return of(createCocktailsFailure({
            error: 'Wrong data'
          }));
        })
      )
    )
  ));

  publishCocktail = createEffect(() => this.actions.pipe(
    ofType(publishCocktailRequest),
    mergeMap(({publish}) => this.cocktailService.publishCocktail(publish).pipe(
      map( () => publishCocktailSuccess()),
      catchError(() => of(publishCocktailFailure({error: 'Not published'})))
    ))
  ));

  deleteCocktail = createEffect(() => this.actions.pipe(
    ofType(deleteCocktailRequest),
    mergeMap(({id}) => this.cocktailService.removeCocktail(id).pipe(
      map(() => deleteCocktailSuccess()),
      tap(() => {
        this.store.dispatch(fetchCocktailsRequest());
      }),
      catchError(() => of(deleteCocktailFailure({error: 'Not deleted'})))
    ))
  ));
}
