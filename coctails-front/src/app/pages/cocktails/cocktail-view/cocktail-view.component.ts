import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCocktailData, Cocktail, PubCock } from '../../../models/cocktail.model';
import { AppState } from '../../../store/types';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { deleteCocktailRequest, fetchCocktailRequest, publishCocktailRequest } from '../../../store/cocktails.actions';

@Component({
  selector: 'app-cocktail-view',
  templateUrl: './cocktail-view.component.html',
  styleUrls: ['./cocktail-view.component.sass']
})
export class CocktailViewComponent implements OnInit {
  cocktail!: Observable<Cocktail | null>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  cocktailId!: ApiCocktailData;
  pubCock!: Observable<boolean>;


  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.cocktail = store.select(state => state.cocktails.cocktail);
    this.loading = store.select(state => state.cocktails.fetchLoading);
    this.error = store.select(state => state.cocktails.fetchError);
    this.pubCock = store.select(state => state.cocktails.pubCock);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let id = params['id'];
      this.store.dispatch(fetchCocktailRequest({id: id}));
    });
    this.cocktail.subscribe(id => {
      this.cocktailId = <ApiCocktailData>id;
    })
  }

  onPublishCocktail(id: string){
    const changeCockStat: PubCock = {
      id: id,
      isPublished: false,
    }
    this.store.dispatch(publishCocktailRequest({publish: changeCockStat}));
  }

  onDelete(id: string){
    this.store.dispatch(deleteCocktailRequest({id}));
  }

}
