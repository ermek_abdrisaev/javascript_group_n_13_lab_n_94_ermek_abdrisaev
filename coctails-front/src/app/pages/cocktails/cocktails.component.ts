import { Component, OnInit } from '@angular/core';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { Cocktail, PubCock } from '../../models/cocktail.model';
import { Observable } from 'rxjs';
import { deleteCocktailRequest, fetchCocktailsRequest, publishCocktailRequest } from '../../store/cocktails.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.sass']
})
export class CocktailsComponent implements OnInit {
  cocktails: Observable<Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  user: Observable<null | User>;

  constructor(private store: Store<AppState>) {
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.loading = store.select(state => state.cocktails.fetchLoading);
    this.error = store.select(state => state.cocktails.fetchError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCocktailsRequest());
  }

  onPublishCocktail(id: string){
    const changeCockStat: PubCock = {
      id: id,
      isPublished: false,
    }
    this.store.dispatch(publishCocktailRequest({publish: changeCockStat}));
  }

  onDelete(id: string){
    this.store.dispatch(deleteCocktailRequest({id}));
  }
}
