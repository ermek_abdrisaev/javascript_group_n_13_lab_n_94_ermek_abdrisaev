import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../../models/user.model';
import { AppState } from '../../../store/types';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { fetchCocktailsRequest, fetchUsersCocktailsRequest } from '../../../store/cocktails.actions';
import { Cocktail } from '../../../models/cocktail.model';

@Component({
  selector: 'app-my-cocktails',
  templateUrl: './my-cocktails.component.html',
  styleUrls: ['./my-cocktails.component.sass']
})
export class MyCocktailsComponent implements OnInit {
  user: Observable<User | null>;
  cocktails: Observable<Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {
    this.user = store.select(state => state.users.user);
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.loading = store.select(state => state.cocktails.fetchLoading);
    this.error = store.select(state => state.cocktails.fetchError);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let id = params['id'];
      this.store.dispatch(fetchUsersCocktailsRequest({id: id}));
    });
  }

}
