import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Cocktail, CocktailData } from '../../models/cocktail.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';
import { createCocktailsRequest, fetchCocktailsRequest } from '../../store/cocktails.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-cocktails-new',
  templateUrl: './cocktails-new.component.html',
  styleUrls: ['./cocktails-new.component.sass']
})
export class CocktailsNewComponent implements OnInit {
  addForm!: FormGroup;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  cocktails: Observable<Cocktail[]>;
  user!: Observable<User | null>;
  userId!: string;

  constructor(
    private store: Store<AppState>
    ) {
    this.loading = store.select(state => state.cocktails.createLoading);
    this.error = store.select(state => state.cocktails.createError);
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCocktailsRequest());
    this.user.subscribe(user =>{
      this.userId = <string>user?._id
    })

    this.addForm = new FormGroup({
      tag: new FormControl('', Validators.required),
      image: new FormControl('', Validators.required),
      recipe: new FormControl('', Validators.required),
      ingredients: new FormArray([]),
    })
  }

  onSubmit(){
    const cocktailData: CocktailData = {
      user: this.userId,
      tag: this.addForm.value.tag,
      image: this.addForm.value.image,
      recipe: this.addForm.value.recipe,
      ingredients: this.addForm.value.ingredients,
      isPublished: false,
    }
    this.store.dispatch(createCocktailsRequest({cocktailData}))
  }

  addIngs(){
    const ings = <FormArray>this.addForm.get('ingredients');
    const ingsGroup = new FormGroup({
      ingName: new FormControl('', Validators.required),
      ingAmmount: new FormControl('',Validators.required),
    });
    ings.push(ingsGroup);
  }

  getIngControlls(){
    const ings = <FormArray>(this.addForm.get('ingredients'));
    return ings.controls;
  }
}
