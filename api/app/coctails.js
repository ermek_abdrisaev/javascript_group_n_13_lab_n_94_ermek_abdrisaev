const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const permit = require("../middlewear/permit");
const auth = require("../middlewear/auth");
const Cocktail = require("../models/Coctail");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const query = {};

    if (req.query.filter === 'image') {
      query.image = {$ne: null};
    }

    if (req.query.user){
      query.user = {_id: req.query.user}
    }

    const cocktails = await Cocktail.find(query).populate('ingredients', 'ingName ingAmmount');

    return res.send(cocktails);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);

    if (!cocktail) {
      return res.status(404).send({message: 'Not found'});
    }

    return res.send(cocktail);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, permit('admin', 'user'), upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.tag || !req.body.recipe) {
      return res.status(400).send({message: 'Tag and recipe are required'});
    }

    const cocktailData = {
      user: req.body.user,
      tag: req.body.tag,
      recipe: req.body.recipe,
      ingredients: JSON.parse(req.body.ingredients),
      isPublished: false,
      image: null,
    };

    if (req.file) {
      cocktailData.image = req.file.filename;
    }

    if( req.user.role === 'admin') {

      cocktailData.isPublished = true;
    }

    const cocktail = new Cocktail(cocktailData);
    await cocktail.save();

    return res.send(
      {message: 'Created new cocktail', id: cocktail._id});
  } catch (e) {
    next(e);
  }
});

router.post('/:id/publish', auth, permit('admin'), async(req, res, next) =>{
  try{
    await Cocktail.updateOne({_id: req.params.id}, {isPublished: req.body.isPublished});
    return res.send({message: 'Published'});
  }catch(e){
    next(e);
  }
});

router.delete('/:id', auth, permit('admin'), async(req, res, next) =>{
  try{
      const cocktail = await Cocktail.deleteOne({_id: req.params.id});
      return res.send(cocktail);
  }catch(e){
    next(e);
  }
})

module.exports = router;