const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  mongo: {
    db: 'mongodb://localhost/cocktails',
    options: {useNewUrlParser: true},
  },
  facebook: {
    appId: '1662884034064421',
    appSecret: '23b6c91b214ab3065457b252b5ba9a8d'
  }
};