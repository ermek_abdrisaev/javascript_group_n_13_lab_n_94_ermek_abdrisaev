const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const IngredientsSchema = new Schema({
  ingName:{
    type: String,
    required: true,
  },
  ingAmmount: String
})


const CocktailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  tag: {
    type: String,
    required: true,
  },
  image: String,
  recipe:{
    type: String,
    required: true
  },
  isPublished: {
    type: Boolean,
    required: true,
    default: false
  },
  ingredients: {
    type: [IngredientsSchema],
    required: true
  },
})


const Cocktail = mongoose.model('Cocktail', CocktailSchema);
module.exports = Cocktail;