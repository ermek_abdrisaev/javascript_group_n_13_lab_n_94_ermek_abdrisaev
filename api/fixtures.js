const mongoose = require('mongoose');
const config = require('./config');
const { nanoid } = require("nanoid");
const User = require('./models/User');
const Cocktail = require('./models/Coctail');

const run = async () =>{
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for(const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@user.com',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'Yusosha'
  }, {
    email: 'admin@admin.com',
    password: '123',
    token: nanoid(),
    role: 'admin',
    displayName: 'Adosha',
    avatar: 'foto.jpg'
  })

   await Cocktail.create({
    user: admin,
    tag: 'Pisco Punch',
    image: 'piscopunch.jpeg',
    ingredients: [{
      ingName: 'pisco',
      ingAmmount: '2oz'
    }],
     recipe: 'Add all ingredients to a cocktail shaker with ice, and shake well on for 8 to 12 seconds. Strain over fresh ice and garnish with a pineapple wedge.',
     isPublished: true,
  }, {
    user: user,
    tag: 'Corpser Viewer',
    image: 'corpserviewer.jpeg',
    ingredients: [{
      ingName: 'London dry gin',
      ingAmmount: '3/4oz',
    }],
     recipe: 'Rinse the inside of a chilled coupe or cocktail glass with absinthe, discard the excess and set the glass aside. Add the gin, Lillet blanc, orange liqueur and lemon juice into a shaker with ice and shake until well-chilled. Strain into the prepared glass.',
     isPublished: false,
  }, {
    user: user,
    tag: 'Sidecar',
    image: 'sidecar.jpeg',
    ingredients: [{
      ingName: 'cognac',
      ingAmmount: '11/2oz'
    }],
     recipe: 'Coat the rim of a coupe glass with sugar, if desired, and set aside. Add the cognac, orange liqueur and lemon juice to a shaker with ice and shake until well-chilled. Strain into the prepared glass. Garnish with an orange twist.',
     isPublished: false,
  }, {
    user: admin,
    tag: 'Blood and sand',
    image: 'bloodandsand.jpeg',
    ingredients: [{
      ingName: 'scotch',
      ingAmmount: '3/4oz, '
    }],
     recipe: 'Add the scotch, sweet vermouth, cherry liqueur and orange juice into a shaker with ice and shake until well-chilled. Strain into a chilled coupe or cocktail glass. Garnish with an orange peel.',
     isPublished: false,
  })


  await mongoose.connection.close();
};

run().catch(e => console.error(e));